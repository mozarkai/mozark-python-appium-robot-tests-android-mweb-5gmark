*** Settings ***
Library           AppiumLibrary    
Resource          ${EXECDIR}/Resources/resource.robot
Variables         ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***
TC_001: Test Wifi Connection
  [Tags]  TestWifiFiberConnection
  LAUNCH BROWSER
  MANAGE WELCOME SCREEN
  go to url  ${SITEURL}
  wait until page contains element  ${TEST_MY_CONNECTION}  timeout=20
  click element  ${TEST_MY_CONNECTION}
  HANDLE LOCATION ALERTS
  click element  ${WIFI_CONNECTION}
  wait until page contains element  ${FIBER_CONNECTION}  timeout=5
  click element  ${FIBER_CONNECTION}
  wait until page contains element  ${DOWNLOAD_RESULT}  timeout=120
  ${download_result}  get text  ${DOWNLOAD_RESULT}
  log to console  Download result is ${download_result}
  ${upload_result}  get text  ${UPLOAD_RESULT}
  log to console  Upload result is ${upload_result}
  quit application