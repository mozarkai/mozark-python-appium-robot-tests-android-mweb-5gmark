*** Settings ***
Library           BuiltIn

*** Keywords ***

LAUNCH BROWSER
    ${browserSession}=    Open Application  ${APPIUM_SERVER}   platformName=android    deviceName=${TEST_DEVICE}   chromedriverPort=${CHROME_PORT}   systemPort=${SYSTEM_PORT}   mjpegServerPort=${MJPEG_PORT}   udid=${TEST_DEVICE}   platformVersion=${PLATFORM_VERSION}    appActivity=${BROWSER_APP_ACTIVITY}    appPackage=${BROWSER_APP_PACKAGE}    newCommandTimeout=0    

MANAGE WELCOME SCREEN
    ${CHROME_WELCOME}  run keyword and return status  wait until page contains element  ${CHROME_WELCOME}  timeout=10
    run keyword if  ${CHROME_WELCOME}==True  CLICK ACCEPT AND CONTINUE
    ${NEXT_BUTTON}  run keyword and return status  wait until page contains element  ${NEXT_BUTTON}  timeout=10
    run keyword if  ${NEXT_BUTTON}==True  CLICK NEXT
    ${NO_THANKS}  run keyword and return status  wait until page contains element  ${NO_THANKS}  timeout=10
    run keyword if  ${NO_THANKS}==True  CLICK NO THANKS    

HANDLE LOCATION ALERTS
    ${location_alert}  run keyword and return status  wait until page contains element  ${LOCATION_PERMISSION}  timeout=10
    run keyword if  ${location_alert}==True  CLICK ALLOW OK
    ${location_allow}  run keyword and return status  wait until page contains element  ${LOCATION_ALLOW}  timeout=10
    run keyword if  ${location_allow}==True  CLICK ALLOW LOCATION 

CLICK ACCEPT AND CONTINUE
    click element  ${ACCEPT_CONTINUE}

CLICK NEXT
    click element  ${NEXT_BUTTON}    

CLICK NO THANKS
    click element  ${NO_THANKS}   

CLICK LOCATION OK
    click element  ${LOCATION_OK}

CLICK ALLOW LOCATION    
    click element  ${LOCATION_ALLOW}