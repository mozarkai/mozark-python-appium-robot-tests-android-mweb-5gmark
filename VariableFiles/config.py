import os

BROWSER_TYPE = 'CHROME'
APPIUM_SERVER = os.environ['APPIUM_SERVER']
PLATFORM_VERSION = os.environ['DEVICE_OS_VERSION']

TEST_DEVICE= os.environ['DEVICE_SERIAL_ID']

CHROME_PORT = os.environ['CHROME_PORT']
SYSTEM_PORT = os.environ['SYSTEM_PORT']
MJPEG_PORT =  os.environ['MJPEG_PORT']

SITEURL = 'https://www.5Gmark.com/test'

#Browser App
BROWSER_APP_ACTIVITY = 'com.google.android.apps.chrome.Main'
BROWSER_APP_PACKAGE = 'com.android.chrome'

# Chrome Config Pages
CHROME_WELCOME = 'com.android.chrome:id/title'
ACCEPT_CONTINUE = 'com.android.chrome:id/terms_accept'
NEXT_BUTTON = 'com.android.chrome:id/next_button'
NO_THANKS = 'com.android.chrome:id/negative_button'

# Test Connection Options
TEST_MY_CONNECTION = "//android.view.View[@text='TEST MY CONNECTION']"
LOCATION_PERMISSION = "//android.widget.TextView[@text='To continue, turn on device location, which uses Google’s location service']"
LOCATION_OK = "//android.widget.Button[@text='OK']"
LOCATION_ALLOW = "//android.widget.Button[@text='Allow']"
WIFI_CONNECTION = "//android.view.View[@text='Wifi']"
ETHERNET_CONNECTION = "//android.view.View[@text='Ethernet']"
FIBER_CONNECTION = "//android.view.View[@text='Fiber']"
OTHER_CONNECTION = "//android.view.View[@text='Other...']"
RTC_OPTION = "//android.view.View[@text='RTC']"

# Results
RESULT_PAGE = "//android.view.View[@text='RESULTS']"
DOWNLOAD_RESULT = '//android.widget.FrameLayout[1]/android.widget.FrameLayout[2]//android.view.View[2]/android.view.View[2]/android.view.View[3]/android.view.View[2]'
UPLOAD_RESULT = '//android.widget.FrameLayout[1]/android.widget.FrameLayout[2]//android.view.View[2]/android.view.View[2]/android.view.View[3]/android.view.View[6]'